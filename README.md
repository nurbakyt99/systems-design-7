# Assignment 7

To run the application locally:

1. Run the docker compose:

    ```shell
    $ cd backend/
    $ docker compose up
    ```
2. [Only the first time] Apply migrations so that your empty DB is populated:

    ```shell
    $ docker-compose exec web /bin/bash
    $ python manage.py migrate
    $ exit
    ```

3. [Only the first time] Create the superuser so you can access Django Admin:

    ```shell
    $ docker-compose exec web /bin/bash
    $ python manage.py createsuperuser
    $ [follow the instructions indicating username & password]
    $ exit
    ```

4. Using the credentials you used in step #3, access the Django Admin at http://localhost:8000/admin/. 