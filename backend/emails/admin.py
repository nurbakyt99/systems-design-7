from django.contrib import admin

from .models import Recipient, RecipientGroup, EmailNotification

admin.site.register(Recipient)
admin.site.register(RecipientGroup)
admin.site.register(EmailNotification)
