from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail


class Recipient(models.Model):
    id = models.AutoField(primary_key=True)
    email = models.EmailField()


class RecipientGroup(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    users = models.ManyToManyField(Recipient)


class EmailNotification(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=1000)
    subject = models.TextField()
    to_group = models.ForeignKey(RecipientGroup, on_delete=models.DO_NOTHING)

@receiver(post_save, sender=EmailNotification)
def send_email_notification(sender, instance, created, **kwargs):
        
        message = Mail(
            from_email=settings.DEFAULT_FROM_EMAIL,
            to_emails=[x.email for x in instance.to_group.users.all()],
            subject=instance.subject,
            html_content=instance.title
        )

        try:
            sg = SendGridAPIClient(settings.SENDGRID_API_KEY)
            response = sg.send(message)
            print("HERE!!!!!!")
            print(response.status_code)
            print(response.body)
            print(response.headers)
            print("!!!!!!!HERE")
        except Exception as e:
            print(str(e))

# Connect the signal
post_save.connect(send_email_notification, sender=EmailNotification)